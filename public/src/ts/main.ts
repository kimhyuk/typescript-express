require("less/main.less");

$(document).ready(function() {
  $(".ui.borderless.main.menu").visibility({
    type: "fixed"
  });
  // create sidebar and attach to menu open
  $(".ui.sidebar")
  .sidebar("attach events", ".toc.item");

  $(".ui.dropdown")
  .dropdown({
    allowAdditions: true
  });
});