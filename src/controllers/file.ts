import { Request, Response, NextFunction } from "express";
import { model as File } from "../models/File";
import path from "path";

export let postUpload = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if (! req.user ) return next("로그인 후 이용해주세요");

        const file = new File({
            target: req.body.target || undefined,
            user_id: req.user._id,
            originalname: req.file.originalname,
            encoding: req.file.encoding,
            mimetype: req.file.mimetype,
            destination: req.file.destination,
            filename: req.file.filename,
            path: req.file.path,
            size: req.file.size,
            isTemp: true
        });

        const fileDoc = await file.save();

        res.send(fileDoc._id);
    } catch (err) {
        return next(err);
    }
};

export let postFile = async ( req: Request, res: Response, next: NextFunction) => {
    // try {
    //     if (!req.query._id && !req.file) throw "파일ID값을 찾을 수 없습니다.";

    //     const fileDoc = <FileModel>await File.findById(req.query._id);

    //     const file = fileDoc ? <FileModel>fileDoc : <FileModel>new File({
    //         target: req.body.target || undefined,
    //         originalname: req.file.originalname,
    //         encoding: req.file.encoding,
    //         mimetype: req.file.mimetype,
    //         destination: req.file.destination,
    //         filename: req.file.destination,
    //         path: req.file.path,
    //         size: req.file.size,
    //         isTemp: false
    //     });


    // } catch (err ) {

    // }
};

export let getUpload = (req: Request, res: Response ) => {
    res.render("test");
};

export let getFile = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if (!req.params._id) throw "FileId is not Found";

        const doc = await File.findById(req.params._id);

        if (!doc) throw "file is not Found";
        res.sendFile(path.join(__dirname, "../../", doc.path));
    } catch (err) {
        return next(err);
    }
};