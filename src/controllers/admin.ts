import async, { find } from "async";
import crypto from "crypto";
import nodemailer from "nodemailer";
import passport from "passport";
import { model as Config, Config as ConfigType }  from "../models/Config";
import { Request, Response, NextFunction } from "express";
import { IVerifyOptions } from "passport-local";
import { WriteError } from "mongodb";
import "../config/passport";
import mongoose from "mongoose";
const request = require("express-validator");



export const getAdmin = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if (!req.user) return res.redirect("/login");
        if (!req.user.isAdmin)  return res.redirect("/");

        res.redirect("/admin/config/main.top");
    } catch (err) {
        return next(err);
    }
};

export const getAdminConfig = async ( req: Request, res: Response, next: NextFunction) => {
    try {
        if (!req.user) return res.redirect("/login");
        if (!req.user.isAdmin)  return res.redirect("/");
        if (!req.params.target) return res.redirect("/admin/config/main.top");

        let confDoc =  await Config.findOne({target: req.params.target });
        console.log(confDoc);
        if ( !confDoc ) {
            confDoc =  new Config({target: req.params.target});
            await confDoc.save();
            console.log(confDoc);
        }
        res.render(`admin/admin.config.pug`, { target: req.params.target , config: confDoc.json ? confDoc.json : {} });
    } catch (err) {
        return next(err);
    }
};

export const postConfig = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if ( !req.user )  return res.redirect("/login");
        else if ( !req.user.isAdmin )  throw "관리자만 접근 가능합니다.";
        const confDoc = await Config.findOne({target: req.body.target});

        const conf = confDoc ? confDoc : new Config({target: req.body.target});

        conf.json = req.body;

        const doc = await conf.save();

        await res.redirect(`/admin/config/${doc.target}`);
    } catch (err) {
        return next(err);
    }
};


export let getTest = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const doc = await Config.findOne({target: "asset"});

    } catch (err) {
        return next(err);
    }
};

