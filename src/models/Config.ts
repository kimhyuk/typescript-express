import mongoose from "mongoose";
import { Timestamp, ObjectID } from "bson";
import { Typegoose, prop } from "typegoose";
import { IConfig } from "../interfaces/Config";

class Config extends Typegoose implements IConfig {
    @prop()
    _id: ObjectID

    @prop()
    target: string

    @prop()
    json: JSON
}

const model = new Config().getModelForClass(Config, { schemaOptions: { timestamps: true}});


export {
    model,
    Config
}