import bcrypt from "bcrypt-nodejs";
import { IUserProfile, IUser } from "../interfaces/User";
import crypto from "crypto";
import {
  Ref,
  pre,
  prop,
  arrayProp,
  Typegoose,
  ModelType,
  InstanceType,
  staticMethod,
  instanceMethod,
  post,
  plugin,
  GetModelForClassOptions
} from "typegoose";
import mongoose from "mongoose";
import mongooseToGraphType  from "mongoose-schema-to-graphql";
import { ObjectID } from "bson";

@pre<User>("save", function save(next) {
  const user = this;
  if (!user.isModified("password")) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, undefined, (err: mongoose.Error, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
})

class User extends Typegoose implements IUser {
  @prop()
  _id: ObjectID
  
  @prop({unique: true})
  email: string
  
  @prop()
  password: string

  @prop()
  passwordResetToken: string

  @prop()
  passwordResetExpires: Date

  @prop()
  facebook: string

  @prop()
  twitter: string

  @prop()
  google: string

  @prop()
  kakao: string

  @arrayProp({items: String})
  tokens: string[]

  @prop()
  profile: IUserProfile

  @prop()
  isAdmin: boolean

  @prop()
  roles: string[]


  @instanceMethod
  comparePassword(this: InstanceType<User>,candidatePassword: string , cb ){
    bcrypt.compare(candidatePassword, this.password, (err: mongoose.Error, isMatch: boolean) => {
      cb(err, isMatch);
    });
  }

  @instanceMethod
  gravatar(this: InstanceType<User>, size: number){
    if (!size) {
      size = 200;
    }
    if (!this.email) {
      return `https://gravatar.com/avatar/?s=${size}&d=retro`;
    }
    const md5 = crypto.createHash("md5").update(this.email).digest("hex");
    return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
  }

  @staticMethod
  static getUserList(this: ModelType<User> & typeof User, skip: number = 0, limit: number = 20){
    return this.find({},{facebook:0,twitter:0,google:0,kakao:0,tokens:0},{skip:skip,limit:limit});
  }
}



type AuthToken = {
  accessToken: string,
  kind: string
};

/**
 * Password hash middleware.
 */

/**
 * Helper method for getting user's gravatar.
 */

// export const User: UserType = mongoose.model<UserType>('User', userSchema);

const model = new User().getModelForClass(User, { schemaOptions: { timestamps: true}});


export {
  AuthToken,
  User,
  model
}