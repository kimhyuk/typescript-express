import {
    Ref,
    prop,
    arrayProp,
    Typegoose,
    ModelType,
    InstanceType,
    staticMethod,
    instanceMethod,
    post,
    plugin,
    GetModelForClassOptions
} from "typegoose";
import { IFile } from "../interfaces/File";
import mongoose from "mongoose";
import mongooseToGraphType  from "mongoose-schema-to-graphql";
import { ObjectID } from "bson";

class File extends Typegoose implements IFile {
    @prop()
    _id: ObjectID

    @prop({index: true})
    target: string

    @prop({index: true})
    targetId: string

    @prop()
    originalname: string

    @prop()
    encoding: string

    @prop()
    mimetype: string

    @prop()
    destination: string

    @prop()
    filename:string

    @prop()
    path:string
    
    @prop()
    size:string

    @prop()
    isTemp: boolean
}




const model = new File().getModelForClass(File, { schemaOptions: { timestamps: true}});


export {
    model,
    File
}
// export const User: UserType = mongoose.model<UserType>('User', userSchema);
// const File = mongoose.model<FileModel>("File", fileSchema);
// export default File;
