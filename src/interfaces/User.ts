import { IBase } from "./Base";

export interface IUserProfile {
    name: string
    gender: string
    location: string
    website: string
    picture: string
}

export interface IGeneralUser extends IBase {
    email: string
    profile: IUserProfile
}

export interface IUser extends IBase, IGeneralUser {
    email: string
    password: string
    passwordResetToken: string
    passwordResetExpires: Date
    facebook: string
    twitter: string
    google: string
    kakao: string
    tokens: string[]
    profile: IUserProfile
    isAdmin: boolean
    roles: string[]
};