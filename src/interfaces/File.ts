import { IBase } from "./Base";

export interface IFile extends IBase {
    target: string
    targetId: string
    originalname: string
    encoding: string
    destination: string
    filename: string
    path: string
    size: string
    isTemp: boolean
}