import { ObjectID } from "bson";

export interface IBase {
    _id : string | ObjectID
    updatedAt?: Date
    createdAt?: Date
}