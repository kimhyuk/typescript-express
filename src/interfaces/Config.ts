import { IBase } from "./base";

export interface IConfig extends IBase {
    target: string,
    json: any
}