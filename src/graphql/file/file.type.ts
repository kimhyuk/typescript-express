import { IFile } from "../../interfaces/File";
import { AuthChecker, ObjectType,Field, ID } from "type-graphql";

@ObjectType()
export class File implements IFile {
    @Field(type => ID)
    _id: string

    @Field()
    target: string

    @Field()
    targetId: string

    @Field()
    originalname: string

    @Field()
    encoding: string

    @Field()
    mimetype: string

    @Field()
    destination: string

    @Field()
    filename:string

    @Field()
    path:string
    
    @Field()
    size:string

    @Field()
    isTemp: boolean
}