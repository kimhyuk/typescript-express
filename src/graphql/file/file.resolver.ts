import { Query, Resolver, Arg, Authorized } from "type-graphql";
import { File } from "./file.type"
import { model as FileModel } from "../../models/File";

@Resolver()
class FileResolver {

    @Authorized("ADMIN")
    @Query(returns => [File])
    async adminFiles(
        @Arg("skip",{ nullable: true}) skip:number = 0,
        @Arg("limit", { nullable: true}) limit:number = 30
    ): Promise<File[]> {
        return await FileModel.find({},{},{skip:skip,limit:limit});
    }

}


export {
    FileResolver
}