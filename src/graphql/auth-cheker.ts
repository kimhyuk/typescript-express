import { AuthChecker } from "type-graphql";

import { IContext } from "../interfaces/context";
import { IUser } from "../interfaces/User";

// create auth checker function
export const authChecker: AuthChecker<IContext> = ({ context: { req } }, roles) => {
    const user = <IUser>req.user;
    if (roles.length === 0) {
    // if `@Authorized()`, check only is user exist
    return req.user !== undefined;
    }
    if (user.isAdmin) {
        return true;
    }
    // there are some roles defined now
    if (!req.user) {
    // and if no user, restrict access
    return false;
    }
    if (user.roles.some(role => roles.includes(role))) {
    // grant access if the roles overlap
    return true;
    }

    // no roles matched, restrict access
    return false;
};