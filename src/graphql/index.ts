import { buildSchemaSync } from "type-graphql";

import { FileResolver } from "./file/file.resolver";
import { UserResolver } from "./user/user.resolver";
import { authChecker } from "./auth-cheker";


const schema = buildSchemaSync({
    resolvers: [FileResolver,UserResolver],
    authChecker: authChecker,
})


export {
    schema
}