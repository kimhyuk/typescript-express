import { IUserProfile, IUser, IGeneralUser } from "../../interfaces/User";
import { InstanceType } from "typegoose";
import { AuthChecker, ObjectType,Field, ID } from "type-graphql";

@ObjectType()
class Profile implements IUserProfile {
    @Field({nullable: true})
    name: string

    @Field({nullable: true})
    gender: string

    @Field({nullable: true})
    location: string

    @Field({nullable: true})
    website: string

    @Field({nullable: true})
    picture: string
}


@ObjectType()
class GeneralUser implements IGeneralUser {
    @Field(type => ID)
    _id: string

    @Field()
    email: string
    
    @Field({nullable: true})
    profile: Profile

}

@ObjectType()
class User extends GeneralUser implements IUser {
    @Field(type => ID)
    _id: string;

    @Field()
    email: string    

    @Field()
    password: string

    @Field({nullable: true})
    passwordResetToken: string

    @Field({nullable: true})
    passwordResetExpires: Date

    @Field({nullable: true})
    facebook: string

    @Field({nullable: true})
    twitter: string
    
    @Field({nullable: true})
    google: string

    @Field({nullable: true})
    kakao: string

    @Field(type => [String],{nullable: true})
    tokens: string[]

    @Field({nullable: true})
    profile: Profile

    @Field()
    isAdmin: boolean

    @Field(type => [String])
    roles: string[]

    @Field()
    updatedAt: Date

    @Field()
    createdAt: Date
}


export {
    User,
    GeneralUser
}