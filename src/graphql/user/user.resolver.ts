import { Query, Resolver, Arg, Authorized } from "type-graphql";
import { User, GeneralUser } from "./user.type"
import { model as UserModel } from "../../models/User";
import { IUser, IGeneralUser } from "../../interfaces/User";

@Resolver()
class UserResolver {

    @Authorized("ADMIN")
    @Query(returns => [User])
    async adminUsers(
        @Arg("skip",{ nullable: true}) skip:number = 0,
        @Arg("limit", { nullable: true}) limit:number = 30
    ): Promise<IUser[]> {
        
        return await UserModel.find({},{},{skip:skip,limit:limit});
    }

    @Query(returns => [GeneralUser])
    async users(
        @Arg("skip",{ nullable: true}) skip:number = 0,
        @Arg("limit", { nullable: true}) limit:number = 30
    ): Promise<IGeneralUser[]> {
        console.log("adsd");
        return await UserModel.find({},{},{skip:skip,limit:limit});
    }
}


export {
    UserResolver
}