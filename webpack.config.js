const webpack = require("webpack");
const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractLESS = new ExtractTextPlugin('[name].css');
const { TsConfigPathsPlugin, CheckerPlugin } = require('awesome-typescript-loader');

module.exports = {
    mode: 'production',
    entry:  {
        main: './public/src/ts/main.ts'
    },
    output: {
        path: path.join(__dirname,"public/dist") ,
        filename: "[name].bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'awesome-typescript-loader?configFileName=./tsconfig.client.json',
                exclude: /node_modules/
            },
            {
                test: /\.less$/i,
                use: extractLESS.extract(['css-loader','less-loader'])
            }
        ]
    },
    plugins: [
        new CheckerPlugin(),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
        }),
        extractLESS
    ],
    optimization: {},
    resolve: {
        alias: {
            js: path.resolve(__dirname,'public/dist/js'),
            css: path.resolve(__dirname,'public/dist/css'),
            less: path.resolve(__dirname, "public/src/less")
        },
        modules:['node_modules'],
        extensions:['.js','.json','.jsx','.css','.ts','.less'],
    }
};